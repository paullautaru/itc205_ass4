package library.returnbook;

import java.util.Date;
import library.borrowbook.BorrowBookControl;
import library.borrowbook.IBorrowBookUI;
import library.entities.ICalendar;
import library.entities.ILibrary;
import library.entities.helpers.BookHelper;
import library.entities.helpers.CalendarFileHelper;
import library.entities.helpers.LibraryFileHelper;
import library.entities.helpers.LoanHelper;
import library.entities.helpers.PatronHelper;
import library.returnbook.IReturnBookUI;
import library.returnbook.ReturnBookControl;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class Bug2SimplificationTest {
    
    @BeforeClass
    public static void setUpClass() throws Exception {
    }
    
    private ICalendar calendar;
    private ILibrary library;
    private ReturnBookControl returnBookControl;
    private BorrowBookControl borrowBookControl;
    
    @Mock IReturnBookUI returnUi;
    @Mock IBorrowBookUI borrowUi;
    
    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        LibraryFileHelper libraryHelper = new LibraryFileHelper(new BookHelper(), new PatronHelper(), new LoanHelper());
        CalendarFileHelper calendarHelper = new CalendarFileHelper();
        
        calendar = calendarHelper.loadCalendar();
        library = libraryHelper.loadLibrary();
        returnBookControl = new ReturnBookControl(library);
        returnBookControl.setUI(returnUi);
        borrowBookControl = new BorrowBookControl(library);
        borrowBookControl.setUI(borrowUi);
        
        //Set up our borrowed book
        borrowBookControl.controlState = borrowBookControl.controlState.SWIPING;
        borrowBookControl.cardSwiped(1);
        borrowBookControl.bookScanned(1);
        borrowBookControl.borrowingCompleted();
        borrowBookControl.commitLoans();
        
        //Make it overdue by one day
        calendar.incrementDate(4);
        library.checkCurrentLoansOverDue();
        Date currentDate = calendar.getDate();
    }

    @Test
    public void testBug1() {
        //arrange
        int bookId = 1;
        double expectedFine = 2.0;
        //execute
        returnBookControl.bookScanned(bookId);
        double actualFine = library.calculateOverDueFine(returnBookControl.currentLoan);
        //assert
        verify(returnUi, atLeast(1)).display(anyString());
        verify(returnUi, atLeast(1)).setState(IReturnBookUI.UIStateConstants.INSPECTING);
        
        assertEquals(expectedFine, actualFine, 0.00);
    }

}
